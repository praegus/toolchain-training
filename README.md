# Toolchain demo set
This project assists in demonstrating toolchain features for use during a PoC or for any other purpose. It contains three different test sets demonstrating how to test API and through web browsers. One test set runs on [VAmPI](https://github.com/erev0s/VAmPI), one test set runs on a custom Praegus [Mendix](https://www.mendix.com/) app. The third testset runs on a local (or remote) variant of [Swag Labs](https://www.saucedemo.com/).

All demo suites can be found under DemoSuite. The link is on the main page after opening the Praegus Open Source Toolchain (POST).

## Requirements
To use the test sets, the scripts are configured to use a local installation of Vampi, SwagLabs and Mendix.

* You can find how to run VAmPI [here](https://github.com/erev0s/VAmPI#run-it).
* If you have access the the custom Praegus Mendix application, download Mendix Modeler 7.23.x and login to the project. Be sure to set the right database through Run > Configuration > databeest.
* Download and run [Swag Labs](https://github.com/saucelabs/sample-app-web), SauceLabs demo application. Please note that this application requires Node.js v14.21.1. Use [nvm](https://github.com/nvm-sh/nvm) to switch versions.
