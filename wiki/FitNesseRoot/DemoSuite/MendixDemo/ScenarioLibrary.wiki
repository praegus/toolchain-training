!|table template|add course                                                    |
|template       |CoursePlannerNewRequest.xml.ftl                               |
|set value      |@{StartDate}        |for          |StartDate                  |
|set value      |@{EndDate}          |for          |EndDate                    |
|set value      |@{Archived}         |for          |Archived                   |
|set value      |@{AllDay}           |for          |AllDay                     |
|set value      |@{Category}         |for          |Category                   |
|set value      |@{RoomName}         |for          |RoomName                   |
|set value      |@{RoomNumber}       |for          |RoomNumber                 |
|set value      |@{RoomCapacity}     |for          |RoomCapacity               |
|set value      |@{CourseName}       |for          |CourseName                 |
|set value      |@{CourseDescription}|for          |CourseDescription          |
|set value      |@{Category}         |for          |Category                   |
|show           |post template to    |http://localhost:8080/ws/CourseWebservice|
|show           |request                                                       |
|show           |response                                                      |
|$status=       |response status                                               |


|scenario  |nieuw scenario|
|doe       |dingen        |
|$varnieuw=|een nieuwe|var|